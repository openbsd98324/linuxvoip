#  LINUX LIVE -- linuxvoip 

Ready to use voip software with a blackbox WM.


# Media 

![](medias/linuxvoip.png)


# Grub





menuentry 'RAMDISK Devuan Live for VOIP, from Partition 2 (Ext3), with fs on sda2' {

set root='hd0,msdos2'

linux  /linuxvoip/vmlinuz boot=live toram=linuxvoip.squashfs  live-media-path=linuxvoip    

initrd /linuxvoip/initrd.img

}




# Checksum


md5sum /linuxvoip/*

255232c122225ba83742fb59bf3e9cfe  linuxvoip/initrd.img

a169e03fd7cc08b66e0849831fa6aa09  linuxvoip/linuxvoip.squashfs

f55229e9a22088ecfb751f359880cbd9  linuxvoip/vmlinuz




